package be.vdab.timesheet.day;

import be.vdab.timesheet.rates.Rates;
import be.vdab.timesheet.slot.BreakSlot;
import be.vdab.timesheet.slot.Slot;
import be.vdab.timesheet.slot.WorkedSlot;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/** This creates an object of WorkedDay
 * This object is an empty array that can be filled with Slots.
 *  You can create this object in the following way:
 * <pre>
 *     Scanner keyboard = new Scanner(System.in);
 *     LocalDate date = LocalDate.parse(keyboard.nextLine());
 *     new WorkedDay(date);
 * </pre>
 * @author Robin en Vincent
 * @version 69
 * @see
 */
public class WorkedDay {
    private LocalDate day;
    private Slot[] slotsOfDay = new Slot[0];
    private Slot chosenSlot;
    private int normalMinutes;
    private int overTimeMinutes;

    public WorkedDay(LocalDate date) {
        setDay(date);
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public LocalDate getDate() {
        return day;
    }


    public Slot[] getSlotsOfDay() {
        return slotsOfDay;
    }

    private void setSlotsOfDay(Slot[] slotsOfDay) {
        this.slotsOfDay = slotsOfDay;
    }

    private void expandArray() {
        Slot[] newSlotsOfDay = new Slot[slotsOfDay.length + 1];
        for (int i = 0; i < slotsOfDay.length; i++) {
            newSlotsOfDay[i] = slotsOfDay[i];
        }
        setSlotsOfDay(newSlotsOfDay);
    }

    private int findEmptySpot() {
        for (int i = 0; i < slotsOfDay.length; i++) {
            if (slotsOfDay[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public void addWorkSlot() {
        Scanner keyboard = new Scanner(System.in);
        LocalTime startTime;
        LocalTime endTime;
        System.out.println("What time would you like the workslot to start at? (hh:mm)");
        try {
            startTime = LocalTime.parse(keyboard.nextLine());
            System.out.println("What time would you like the workslot to end at? (hh:mm)");
            endTime = LocalTime.parse(keyboard.nextLine());
            if (startTime.isBefore(endTime)) {
                WorkedSlot workSlot = new WorkedSlot(startTime, endTime);

                int index = findEmptySpot();
                if (index == -1) {
                    index = slotsOfDay.length;
                    expandArray();
                }
                slotsOfDay[index] = workSlot;
            } else {
                System.out.println("The start time cannot be after the end time!");
                addWorkSlot();
            }
        } catch (DateTimeParseException dateTimeParseException) {
            System.out.println("The time format you've entered is incorrect (eg:. 08:30)");
            addWorkSlot();
        }
    }

    private int breakSlotLocation(){
        int chosenBreakSlotLocation =0;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Where would you like to add a break?");

        for (int i = 0; i < slotsOfDay.length ; i++) {
            System.out.println((i + 1) + ". " + slotsOfDay[i]);
        }

        try {
            int madechoice = Integer.parseInt(keyboard.nextLine());
            if (madechoice >= 1 && madechoice <= slotsOfDay.length) {
                chosenBreakSlotLocation = madechoice - 1;
            } else {
                System.out.println("Please enter a number between 1 and " + slotsOfDay.length);
                breakSlotLocation();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a valid number");
            breakSlotLocation();
        }
        return chosenBreakSlotLocation;
    }


    public void addBreakSlot() {
        Scanner keyboard = new Scanner(System.in);
        int chosenWorkSlot = breakSlotLocation();
        LocalTime startTime;
        LocalTime endTime;


        System.out.println("What time would you like the breakslot to start at? (hh:mm)");
        try {
            startTime = LocalTime.parse(keyboard.nextLine());
            if (startTime.isBefore(slotsOfDay[chosenWorkSlot].getStart()) || startTime.isAfter(slotsOfDay[chosenWorkSlot].getEnd())){
                System.out.println("You can't take a break when you're not working!");
                addBreakSlot();
                return;
            }
            System.out.println("What time would you like the breakslot to end at? (hh:mm)");
            endTime = LocalTime.parse(keyboard.nextLine());
            if (endTime.isBefore(startTime) || endTime.isAfter(slotsOfDay[chosenWorkSlot].getEnd())){
                System.out.println("Your break can't end when not working!");
                addBreakSlot();
                return;
            }
            BreakSlot breakSlot = new BreakSlot(startTime, endTime);
            int index = findEmptySpot();
            if (index == -1) {
                index = slotsOfDay.length;
                expandArray();
            }

            slotsOfDay[index] = breakSlot;

        } catch (DateTimeParseException dateTimeParseException) {
            System.out.println("The time format you've entered is incorrect (eg:. 08:30)");
            addBreakSlot();
            return;
        }
    }

    public void remove() {
        selectSlot();
        for (int i = 0; i < slotsOfDay.length; i++) {
            if (slotsOfDay[i].equals(chosenSlot)) {
                slotsOfDay[i] = null;
                break;
            }
        }
    }

    private void selectSlot() {
        Scanner slotSelecter = new Scanner(System.in);
        System.out.println("Please enter a number corresponding with the timeslot you would like to modify:");
        for (int i = 0; i < slotsOfDay.length; i++) {
            System.out.printf("%d: %s\n", i + 1, slotsOfDay[i]);
        }
        try {
            int madechoice = Integer.parseInt(slotSelecter.next());
            if (madechoice >= 1 && madechoice <= slotsOfDay.length) {
                chosenSlot = slotsOfDay[madechoice - 1];
            } else {
                System.out.println("Please select a timeslot");
                selectSlot();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a valid number");
            selectSlot();
        }
    }

    public float brutoDay() {
        return timedBruto(overTimeMinutes, true) + timedBruto(normalMinutes, false);
    }

    private float timedBruto(int timeType, boolean isOverTime) {
        calculateTime();
        float bruto = 0;
        switch (day.getDayOfWeek()) {
            case SUNDAY:
                bruto = bruto + (((timeType) / 60f) * Rates.getRate(Rates.SUNDAY));
                return bruto;
            case SATURDAY:
                bruto = bruto + (((timeType) / 60f) * Rates.getRate(Rates.SATURDAY));
                return bruto;
            default:
                if (isOverTime) {
                    bruto = timeType / 60f * Rates.getRate(Rates.OVERTIME);
                } else {
                    bruto = timeType / 60f * Rates.getRate(Rates.NORMAL);
                }
                return bruto;
        }
    }

    private void calculateTime() {
        normalMinutes = 0;
        overTimeMinutes = 0;
        for (Slot s : slotsOfDay) {
            if (s instanceof WorkedSlot) {
                int[] normalRateMinutesAndOverTimeMinutes = s.timeInSlot();
                normalMinutes = normalMinutes + normalRateMinutesAndOverTimeMinutes[0];
                overTimeMinutes = overTimeMinutes + normalRateMinutesAndOverTimeMinutes[1];
            } else if (s instanceof BreakSlot) {
                int[] normalRateMinutesAndOverTimeMinutes = s.timeInSlot();
                normalMinutes = normalMinutes - normalRateMinutesAndOverTimeMinutes[0];
                overTimeMinutes = overTimeMinutes - normalRateMinutesAndOverTimeMinutes[1];
            }
        }
    }

    @Override
    public String toString() {
        calculateTime();
        return "NormalMinutes worked: " + normalMinutes + ", earned: " + timedBruto(normalMinutes, false) + "\t\t" +
                "OverTimeMinutes worked:" + overTimeMinutes + ", earned: " + timedBruto(overTimeMinutes, true) + "\t\t" +
                "Totalbrutto: " + brutoDay() + ".\n";
    }

    public void showDetailedDay() {
        for (Slot slot : slotsOfDay) {
            int[] normalRateMinutesAndOverTimeMinutes = slot.timeInSlot();
            if (slot instanceof WorkedSlot) {
                System.out.printf(slot +
                        "\t\tEarned at normal rate: $" +
                        timedBruto(normalRateMinutesAndOverTimeMinutes[0], false) +
                        "\t\tEarned at overtime rate: $" + timedBruto(normalRateMinutesAndOverTimeMinutes[1], true) +
                        ".\n");
            } else if (slot instanceof BreakSlot) {
                System.out.printf(slot +
                        "\t\tLost at normal rate: $" +
                        timedBruto(normalRateMinutesAndOverTimeMinutes[0], false) +
                        "\t\t\tLost at overtime rate: $" + timedBruto(normalRateMinutesAndOverTimeMinutes[1], true) +
                        ".\n");
            }
        }
    }
}
