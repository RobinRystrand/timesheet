package be.vdab.timesheet;

import be.vdab.timesheet.rates.Rates;
import be.vdab.timesheet.week.WorkedWeek;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/** This is the menu that opens and allows you to access the functionalities of the program.
 *  This can be created in the following way:
 *  <pre>
 *      Menu menu = new Menu();
 *  </pre>
 * @author Robin en Vincent
 * @version 69
 * @see
 */
public class Menu {
    private Scanner keyboard = new Scanner(System.in);
    private WorkedWeek workedWeek;
    private int choice;
    private int slotsCreated =0;


    /** This opens the options you can select from.
     * This can be used in the following way:
     * <pre>
     *     menu.startMenu();
     * </pre>
     * @author Robin en Vincent
     * @version 69
     * @see
     */
    public void startMenu() {
        System.out.println("Welcome to our timesheet app.");
        System.out.println("We are booting up the system for you.");
        System.out.println("What would you like to do?");
        System.out.print(
                "1. Different hourly rates\n" +
                "2. Start a new workweek\n" +
                "3. Add worked moment or break\n" +
                "4. remove worked moment or break\n" +
                "5. Reset\n" +
                "6. Print paycheck\n" +
                "7. Print detailed paycheck\n" +
                "8. Quit application\n"
        );
        System.out.println("Type in the number that corresponds with your choice");
        selection();

        switch (choice){
            case 1:
                Rates.showRates();
                break;
            case 2:
                //make this editable
                Scanner specialKeyboard = new Scanner(System.in);
                System.out.println("When would you like the week to start? (yyyy-mm-dd)");
                try {
                    workedWeek = new WorkedWeek(LocalDate.parse(specialKeyboard.nextLine()));
                } catch (DateTimeParseException dateTimeParseException){
                    System.out.println("The date should be in the format \"yyyy-mm-dd\" (eg: 2001-09-11)");
                }
                break;
            case 3:
                //add worked moment or break
                if (workedWeek != null){
                workedWeek.selectDay();
                workedWeek.workOrBreak();
                workedWeek.addSlot();
                slotsCreated++;
                } else {
                    System.out.println("Please start a workweek first.");
                }
                break;
            case 4:
                //remove worked moment or break
                if (slotsCreated > 0) {
                    workedWeek.selectDay();
                    workedWeek.removeSlot();
                    slotsCreated--;
                } else {
                    System.out.println("Please add a worked moment or break first.");
                }
                break;
            case 5:
                //reset
                workedWeek = null;
                slotsCreated = 0;
                break;
            case 6:
                //print paycheck
                if (slotsCreated > 0) {
                    workedWeek.simplePaycheck();
                }else {
                    System.out.println("Please add a worked moment or break first.");
                }
                break;
            case 7:
                //print detailed paycheck
                if (slotsCreated > 0) {
                workedWeek.detailedPaycheck();
                } else {
                    System.out.println("Please add a worked moment or break first.");
                }
                break;
            case 8:
                System.out.println("Thanks for using our app, see you soon!");
                System.exit(0);
                break;
            default:
                break;
        }
    }
    private void selection() {
        try {
            int madechoice = Integer.parseInt(keyboard.next());
            if (madechoice >=1 && madechoice <=8) {
                choice = madechoice;
            } else {
                System.out.println("Please enter a number between 1 and 8");
                selection();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a valid number");
            selection();
        }
    }
}
