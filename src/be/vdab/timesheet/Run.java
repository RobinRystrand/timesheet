package be.vdab.timesheet;

/** This class starts the program.
 * It creates a menu and opens it
 * Then it loops until the program is closed
 * This should only be used in MAIN
 * This can be called in the following way:
 * <pre>
 *     new Run();
 * </pre>
 * @author Robin en Vincent
 * @version 69
 * @see
 */
public class Run {
    public Run(){
        Menu menu = new Menu();
        while (true){
            menu.startMenu();
        }
    }
}
