package be.vdab.timesheet.week;

import be.vdab.timesheet.day.WorkedDay;

import java.time.LocalDate;
import java.util.Scanner;


/** This class is a week, consisting of worked days
 * This class can be used the following way:
 * <pre>
 *     WorkedWeek workweek = new WorkedWeek("2001-09-11");
 * </pre>
 * @author Robin en Vincent
 * @version 69
 * @see be.vdab.timesheet.day.WorkedDay
 */
public class WorkedWeek {
    private final WorkedDay[] workweek = new WorkedDay[7];
    private int chosenDay;
    private int typeOfSlot;

    public WorkedWeek(LocalDate date) {
        for (int i = 0; i < 7; i++) {
            workweek[i] = new WorkedDay(date.plusDays(i));
        }
    }

    public void addSlot(){
        if (typeOfSlot == 1){
        workweek[chosenDay].addWorkSlot();
        } else if (typeOfSlot == 2){
            workweek[chosenDay].addBreakSlot();
        }
    }

    public void selectDay(){
        Scanner daySelecter = new Scanner(System.in);
        System.out.println("Please enter a number corresponding with the day you would like to modify:");
        for (int i = 0; i < 7; i++){
            System.out.printf("%d: %s\n", i+1, workweek[i].getDate().getDayOfWeek());
        }
        try {
            int day = Integer.parseInt(daySelecter.next());
            if (day >=1 && day <=7) {
                chosenDay = day -1;
            } else {
                selectDay();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a valid day");
            selectDay();
        }
    }

    public void workOrBreak(){
        if (workweek[chosenDay].getSlotsOfDay().length > 0){
        Scanner slotSelecter = new Scanner(System.in);
        System.out.println("Please enter a number corresponding with the type you would like to add:");
        System.out.println("1: Worked time");
        System.out.println("2: Break time");

        try {
            int slot = Integer.parseInt(slotSelecter.next());
            if (slot >=1 && slot <=2) {
                 typeOfSlot = slot;
            } else {
                workOrBreak();
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a valid type");
            workOrBreak();
        }
        } else {
            typeOfSlot = 1;
        }
    }

    public void removeSlot(){
        workweek[chosenDay].remove();
    }

    public void simplePaycheck(){
        float totalBruto = 0;
        for (WorkedDay workday:workweek) {
            System.out.printf("%10s\t\t" + workday, workday.getDate().getDayOfWeek());
            totalBruto += workday.brutoDay();
        }
        System.out.printf("This weeks bruto wage is \t$%.2f.\n", totalBruto);
        System.out.printf("VAT: \t\t\t\t\t\t$%.2f.\n",(totalBruto * 0.21f));
        System.out.printf("This weeks netto wage is \t$%.2f.\n\n",(totalBruto - (totalBruto * 0.21f)));
    }

    public void detailedPaycheck(){
        float totalBruto = 0;
        for (WorkedDay workday:workweek) {
            System.out.println(workday.getDate().getDayOfWeek());
            workday.showDetailedDay();
            System.out.println(workday);
            totalBruto += workday.brutoDay();
        }
        System.out.printf("This weeks bruto wage is \t$%.2f.\n", totalBruto);
        System.out.printf("VAT: \t\t\t\t\t\t$%.2f.\n",(totalBruto * 0.21f));
        System.out.printf("This weeks netto wage is \t$%.2f.\n\n",(totalBruto - (totalBruto * 0.21f)));
    }
}

