package be.vdab.timesheet.rates;

import java.util.Locale;

public enum Rates {
    NORMAL,
    OVERTIME ,
    SATURDAY,
    SUNDAY;
    public static float getRate(Rates rates) {
        switch (rates) {
            case NORMAL: return 15f;
            case OVERTIME: return 20f;
            case SATURDAY: return 25f;
            case SUNDAY: return 30f;
        }
        return 0;
    }
    public static void showRates(){
        for (Rates rates: Rates.values()) {
            System.out.printf("The " + rates.toString().toLowerCase(Locale.ROOT) + " rates are $%.2f per hour.\n",getRate(rates) );
        }
    }
}
