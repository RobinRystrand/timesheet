package be.vdab.timesheet.slot;

import java.time.LocalTime;
import static java.time.temporal.ChronoUnit.MINUTES;

/** This creates a Slot.
 * Every slot contains a start and end moment.
 * Every slot can calculate how many minutes a slot lasts and how much of it is overtime or not.
 * @author Robin en Vincent
 * @version 69
 * @see
 */
public interface Slot {
    /** This defines when normal rates start*/
    LocalTime WORK_DAY_START = LocalTime.of(8,00);
    /** This defines when normal rates end */
    LocalTime WORK_DAY_END = LocalTime.of(18,00);

    /** Returns the start time of the slot in LocalTime.
     * @return a LocalTime of when the Slot starts.
     */
    LocalTime getStart();
    /** Returns the end time of the slot in LocalTime.
     * @return a LocalTime of when the Slot ends.
     */
    LocalTime getEnd();

    /** Returns an array which contains the total minutes worked
     * @return an int array of size 2, with at index 0 normal rate minutes and at index 1 overtime rate minutes.
     */
    default int[] timeInSlot() {
        int[] timeWorked = new int[2];
        if (this.getStart().isBefore(WORK_DAY_START)) {
            if (this.getEnd().isBefore(WORK_DAY_START)) {
                timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(this.getStart(), this.getEnd()));
            } else if (this.getEnd().isAfter(WORK_DAY_END)) {
                timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(this.getStart(), WORK_DAY_START));
                timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(WORK_DAY_END, this.getEnd()));
                timeWorked[0] = timeWorked[0] + Math.toIntExact(MINUTES.between(WORK_DAY_START, WORK_DAY_END));
            } else {
                timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(this.getStart(), WORK_DAY_START));
                timeWorked[0] = timeWorked[0] + Math.toIntExact(MINUTES.between(WORK_DAY_START, this.getEnd()));
            }
        } else if (this.getStart().isAfter(WORK_DAY_END)) {
            timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(this.getStart(), this.getEnd()));
        } else if (this.getStart().isBefore(WORK_DAY_END)) {
            if (this.getEnd().isAfter(WORK_DAY_END)) {
                timeWorked[1] = timeWorked[1] + Math.toIntExact(MINUTES.between(WORK_DAY_END, this.getEnd()));
                timeWorked[0] = timeWorked[0] + Math.toIntExact(MINUTES.between(this.getStart(), WORK_DAY_END));
            } else {
                timeWorked[0] = timeWorked[0] + Math.toIntExact(MINUTES.between(this.getStart(), this.getEnd()));
            }
        }
        return timeWorked;
    }
}
