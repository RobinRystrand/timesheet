package be.vdab.timesheet.slot;

import java.time.LocalTime;

/** This creates a Slot where the user has worked.
 * This can be used in the following way:
 * <pre>
 *     new WorkedSlot(Localtime startTime, LocalTime endTime);
 * </pre>
 * The start- and endtimes need to be LocalTime objects, these can be made in any way you want.
 * @author Robin en Vincent
 * @version 69
 * @see be.vdab.timesheet.slot.Slot
 */
public class WorkedSlot implements Slot {
    private LocalTime start;
    private LocalTime end;

    public WorkedSlot(LocalTime startTime, LocalTime endTime) {
        start = startTime;
        end = endTime;
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    @Override
    public LocalTime getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "WorkedSlot: " +
                "start=" + start +
                ", end=" + end;
    }

}
