package be.vdab.timesheet.slot;

import java.time.LocalTime;

/** This creates a Slot where the user has taken a break.
 * This can be used in the following way:
 * <pre>
 *     new BreakSlot(Localtime startTime, LocalTime endTime);
 * </pre>
 * The start- and endtimes need to be LocalTime objects, these can be made in any way you want.
 * @author Robin en Vincent
 * @version 69
 * @see be.vdab.timesheet.slot.Slot
 */
public class BreakSlot implements Slot{
    private LocalTime start;
    private LocalTime end;

    public BreakSlot(LocalTime startTime, LocalTime endTime){
        start = startTime;
        end = endTime;
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    @Override
    public LocalTime getEnd() {
        return end;
    }


    @Override
    public String toString() {
        return "BreakSlot: " +
                "start=" + start +
                ", end=" + end;
    }

}
